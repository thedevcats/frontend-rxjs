import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Champion } from 'src/app/models/champion';
import { Observable } from 'rxjs';
import { CoreStore } from '../../stores/core/core.store';

@Injectable({
  providedIn: 'root'
})
export class DetailsResolver implements Resolve<any> {
  selectedChampion: Champion;
  constructor(private coreStore: CoreStore) {
    this.coreStore.select('selectedChampion').subscribe(d => (this.selectedChampion = d));
  }

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.id;

    if (!this.selectedChampion || this.selectedChampion.id !== id) {
      this.coreStore.getChampionById(id);
    }
  }
}
