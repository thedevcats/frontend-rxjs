import { Injectable } from '@angular/core';
import { Store } from './_base.store';
import { Champion } from 'src/app/models/champion';
import { ApiService } from '../../services/api/api.service';
import { Router } from '@angular/router';

export class CoreState {
  champions: Array<Champion> = [];
  selectedChampion: Champion = null;
}

@Injectable({
  providedIn: 'root'
})
export class CoreStore extends Store<CoreState> {
  constructor(private apiService: ApiService, private router: Router) {
    super(new CoreState());
  }

  getAllChampions(): void {
    this.apiService.getAllChampions().subscribe((champions: Array<Champion>) => {
      this.setState({
        ...this.state,
        champions
      });
    });
  }

  getChampionById(id: string): void {
    this.apiService.getChampionById(id).subscribe((selectedChampion: Champion) => {
      this.setState({
        ...this.state,
        selectedChampion
      });
      this.router.navigate(['champions', id]);
    });
  }
}
