import { Observable, BehaviorSubject, ConnectableObservable } from 'rxjs';
import { pluck, map, publish, distinctUntilChanged } from 'rxjs/operators';

export class Store<T> {
  private _state$: BehaviorSubject<T>;
  public state$: Observable<T>;

  protected constructor(initialState: T) {
    this._state$ = new BehaviorSubject(initialState);
    this.state$ = this._state$.asObservable();
  }

  get state(): T {
    return this._state$.getValue();
  }

  setState(nextState: T): void {
    console.log('%c >>> New State >>>', 'background: #222; color: #bada55', nextState);
    this._state$.next(nextState);
  }

  select(prop: string): any {
    return this.state$.pipe(
      map(state => state[prop]),
      distinctUntilChanged()
    );
  }

  selectObs(prop: string): any {
    const obs = this.state$.pipe(pluck(prop), publish()) as ConnectableObservable<any>;
    obs.connect();
    return obs;
  }
}
