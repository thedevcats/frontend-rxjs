import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChampionDashboardComponent } from './components/champion-dashboard/champion-dashboard.component';
import { ChampionDetailsComponent } from './components/champion-details/champion-details.component';
import { DetailsResolver } from './core/resolvers/details/details.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'champions', pathMatch: 'full' },
  {
    path: 'champions',
    component: ChampionDashboardComponent
  },
  {
    path: 'champions/:id',
    component: ChampionDetailsComponent,
    resolve: { data: DetailsResolver }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
