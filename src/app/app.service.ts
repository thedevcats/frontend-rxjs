import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Champion } from './models/champion';
import { CoreStore } from './core/stores/core/core.store';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  champions$: Observable<Array<Champion>>;
  selectedChampion$: Observable<Champion>;

  constructor(private coreStore: CoreStore) {
    this.coreStore.getAllChampions();
    this.champions$ = this.coreStore.select('champions');
    this.selectedChampion$ = this.coreStore.select('selectedChampion');
  }

  navigateToDetails(id: string) {
    this.coreStore.getChampionById(id);
  }
}
